# pan

#### 介绍
Pan局域网共享软件1.0
- 简单 高效 快捷！
- 官方地址 https://aqzt.com/5796.html

#### 软件架构
Pan局域网共享软件是一款可以在局域网范围内文件传输的工具，能让你在局域网范围内免费进行文件传输，是目前最省时省事的局域网文件共享神器。支持多平台 windows/linux/arm等系统，可以快速实现文件共享。


#### 安装教程

1.  下载软件，如果是压缩包，解压即可
2.  无需安装，双击运行即可
3.  手机系统，开启ssh后，使用Pan-linux-armv7-1.0版本也可以运行


#### 使用说明

1.  双击Pan-windows-amd64-1.0.exe运行,即可共享当前目录下的文件
![image](https://gitee.com/go2019/pan/raw/master/img/pan1.png)

2.  其他电脑使用浏览器访问，地址http://192.168.9.111/，使用的IP为你的局域网IP
![image](https://gitee.com/go2019/pan/raw/master/img/pan2.png)

3.  停止共享，关闭窗口即可


